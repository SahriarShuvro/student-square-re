document.getElementById("footer").innerHTML = `
<footer>
      <div class="f-top">
        <div class="row">
          <div class="col-md-4">
            <div class="logo-f-sec">
              <a href="./index.html" class="btn">
                <img src="./src/img/SSLandscapeLogo.png" alt="" srcset="" />
              </a>
              <p class="short-dis">
                Student Square is a platform for educational counselling and
                career development for students.
              </p>
              <a href="./donation.html" class="btn becomeDonor">
                Become A Donor
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="menu-f-sec">
              <p class="m-f-head">Organization</p>
              <div class="f-menus">
                <div class="f-m-left">
                  <a href="./about-us.html">About Us</a>
                  <a href="./press.html">Press</a>
                  <a href="./careers.html">Careers</a>
                </div>
                <div class="f-m-right">
                  <a href="./terms-use.html">Terms Of Use</a>
                  <a href="./privacy-policy.html">Privacy Policy</a>
                  <a href="./refund-policy.html">Refund Policy</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="contact-f-sec">
              <p class="contact-head">Contact Us At</p>
              <div class="c-f-top">
                <p>Call: <a href="tel:+23234">23234</a> (9AM - 10PM)</p>
                <p>SMS: SSHelp to 23234</p>
                <p>
                  Email:
                  <a href="mailto:support@studentsquare.com"
                    >support@studentsquare.com</a
                  >
                </p>
              </div>
              <div class="c-f-bottom">
                <a href="#"><i class="fab fa-facebook-f"></i></a>
                <a href="#"><i class="fab fa-youtube"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
                <a href="#"><i class="fab fa-linkedin"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="f-bottom">
        <div class="row">
          <div class="col-md-4">
            <div class="get-app">
              <a href="#">
                <img src="./src/img/s-8/getAppStore.png" alt="" srcset="" />
              </a>
              <a href="#">
                <img src="./src/img/s-8/getGooglePlay.png" alt="" srcset="" />
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="designed-dev">
              <p>
                Designed & Developed By
                <a
                  href="http://www.consoledge.rf.gd"
                  target="_blank"
                  rel="noopener noreferrer"
                  class="text-warning"
                  >ConsolEDGE</a
                >
              </p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="copyright">
              <p>©2023, Student Square. All Rights Reserved</p>
            </div>
          </div>
        </div>
      </div>
    </footer>

`;
