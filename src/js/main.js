// Nav Bar Start

import { initializeNavBar } from "./_nav-bar.js";

// For a browser
window.addEventListener("DOMContentLoaded", (event) => {
  initializeNavBar();
});

// Nav Bar End
